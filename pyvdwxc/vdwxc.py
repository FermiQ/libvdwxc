import os
from ctypes import c_int, c_double, c_long, Structure, pointer, POINTER, byref, \
     c_byte, CDLL
from ctypes.util import find_library

import numpy as np


FUNC_VDWDF = 1


class LibVDWXCError(Exception):
    pass


def find_vdw_library():
    envvar = 'PYVDWXC_LIBRARY'
    path = os.environ.get(envvar)
    if path is None:
        path = find_library('vdwxc')
    # Check LD_LIBRARY_PATH?  Not searched by ctypes.util.find_library.
    if path is None:
        raise LibVDWXCError('Could not find libvdwxc.  Please check the '
                            'installation.  You may specify its full path '
                            'as the environment variable "%s".' % envvar)
    return CDLL(path)


class VDWXC:
    def __init__(self, N_c, cell_cv):
        lib = find_vdw_library()

        # Size of the struct which represents vdW calculation.
        data_nbytes = lib.vdw_df_data_get_size()

        class VDWData(Structure):
            _fields_ = [('data', c_byte * data_nbytes)]
        VDWDataPointer = POINTER(VDWData)

        def arraytype(**kwargs):
            return np.ctypeslib.ndpointer(flags='C_CONTIGUOUS', **kwargs)

        c_stdarray = arraytype(dtype=np.float64,
                               ndim=3)
        # To avoid confusing __del__ in case of crash in constructor
        self.lib = None

        # Sort of private functions
        types = [c_int, c_double, c_double] + 5 * [c_stdarray]
        lib.calculate_q0.argtypes = types
        types = [c_int] + 3 * [c_stdarray]
        lib.calculate_thetas.argtypes = types
        types = [c_int] + 3 * [c_stdarray]
        lib.calculate_spline_values_and_derivatives.argtypes = types

        # Public functions
        types = [VDWDataPointer, c_int]
        lib.vdw_df_initialize.argtypes = types
        types = [VDWDataPointer] + 3 * [c_int] + 9 * [c_double]
        lib.vdw_df_set_unit_cell.argtypes = types
        #types = [VDWDataPointer, c_int, arraytype(dtype=np.intc)]
        #lib.vdw_df_set_topology.argtypes = types
        types = [VDWDataPointer] + 4 * [c_stdarray]
        lib.vdw_df_calculate.argtypes = types
        lib.vdw_df_calculate.restype = c_double
        types = [VDWDataPointer]
        lib.vdw_df_finalize.argtypes = types

        self._pdata = pointer(VDWData())
        lib.vdw_df_initialize(self._pdata, FUNC_VDWDF)
        self.lib = lib

        self._set_unit_cell(N_c, cell_cv)
        #self.gshape = None  #complete box shape
        #self.ngpts = None   #number of point
        #self.lshape = None  #local shape, used for MPI

    def _set_unit_cell(self, N_c, cell_cv):
        assert len(N_c) == 3
        cell = np.ravel(cell_cv)
        if len(cell) == 3:
            cell = np.diag(cell).ravel()
        assert len(cell) == 9
        gshape = tuple(map(int, N_c))
        #self.lshape = tuple(map(int, lshape))
        #self.ngpts = np.prod(self.lshape)
        args = list(gshape)
        #args.append(self.lshape[0])
        args += map(c_double, cell)
        self.lib.vdw_df_set_unit_cell(self._pdata, *args)

    def calculate_q0(self, Z_ab, qcut, rho_g,
                     sigma_g, q0_g, dq0_drho_g,
                     dq0_dsigma_g):
        self.lib.calculate_q0(rho_g.size, Z_ab, qcut,
                              rho_g, sigma_g, q0_g,
                              dq0_drho_g, dq0_dsigma_g)

    def calculate_thetas(self, rho_g, q0_g, thetas_ga):
        self.lib.calculate_thetas(len(rho_g), rho_g,
                                  q0_g, thetas_ga)

    def calculate(self, rho_g, sigma_g):
        #assert rho_g.shape == self.lshape, (rho_g.shape, self.lshape)
        #assert sigma_g.shape == self.lshape
        dedrho_g = np.zeros_like(rho_g)
        dedsigma_g = np.zeros_like(rho_g)
        # XXX test properly
        # missing argument, expected MPI_Comunicator in pos 6
        ## FIXME description interface in class constructor, to match
        energy = self.lib.vdw_df_calculate(self._pdata,
                                           rho_g,
                                           sigma_g,
                                           dedrho_g,
                                           dedsigma_g)
        return energy, dedrho_g, dedsigma_g

    def __del__(self):
        # May or may not be initialized
        if hasattr(self, 'lib'):
            self.lib.vdw_df_finalize(self._pdata)


def vdwmain():
    import numpy as np
    #n = 5000
    dims = (16, 16, 16)
    rho = np.zeros(dims, dtype=float)
    #rho = np.linspace(0.0, 4.0, n) # zero?
    sigma = rho / 1000.0
    q0 = np.empty_like(rho)
    dq0_drho = np.empty_like(rho)
    dq0_dgradrho = np.empty_like(rho)

    #thetas_ga = np.zeros((n, 20))

    for x in [q0, dq0_drho, dq0_dgradrho]:
        x[:] = np.nan

    Z_ab = -0.8491
    qcut = 5.0

    # Load library
    vdw = VDWXC()
    N_c = np.array(dims, dtype=int)
    cell_cv = np.array([[5.0, 0., 0.],
                        [0., 5.0, 0.],
                        [0., 0., 5.4]])
    vdw.set_unit_cell(N_c, cell_cv)

    n_g = np.zeros(N_c, dtype=float)
    sigma_g = n_g.copy()

    #vdw.set_topology(np.arange(n_g.size))

    import pylab as pl

    #vdw.calculate(n_g, sigma_g)
    qval = np.linspace(0.0, 1.0, 1000)**2 * 5
    out = np.zeros((len(qval), 20), dtype=float)
    print 'call eval'
    for i, q in enumerate(qval):
        vdw.lib.evaluate_palpha_splines(c_double(1.0),
                                        c_double(q),
                                        darray(out[i]))

    if 0:
        for j, o in enumerate(out.T):
            pl.plot(qval, o)
        print 'ok'

        pl.show()

        #print lib.evaluate_palpha_splines

        #vdw_df_calculate(&vdwdf, rho_i, sigma_i, v_i);
        return

    # Use library method
    vdw.calculate_q0(Z_ab, qcut, rho, sigma, q0, dq0_drho, dq0_dgradrho)

    #import pylab as pl
    #for qslice in q0:
    #    pl.matshow(qslice)
    #pl.show()

    #vdw.calculate_vdw_df_energy(rho, sigma)
    return
    #q0 = np.linspace(0,5.0, n)
    rho = np.ones(n)
    vdw.calculate_thetas(rho, q0, thetas_ga)
    f = open('plot.txt', 'w')
    for i in range(n):
        print >>f, q0[i],
        for q in range(20):
            print >>f, thetas_ga[i][q],
        print >>f
    f.close()

    f = open('q0test.output','w')
    for i in range(len(rho)):
        print >>f, "%.15f" % rho[i], "%.15f" % sigma[i], "%.15f" % q0[i], "%.15f" % dq0_drho[i], "%.15f" % dq0_dgradrho[i]
    f.close()


if __name__ == '__main__':
    vdwmain()
