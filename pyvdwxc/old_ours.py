import numpy as np
from numpy.fft import ifftn, fftn, rfftn
from scipy.interpolate import UnivariateSpline as ScipySpline
from ase.io.cube import read_cube
from gpaw.grid_descriptor import GridDescriptor
from gpaw.utilities.tools import construct_reciprocal
from gpaw.xc.gga import GGA
from gpaw.xc.libxc import LibXC
import warnings
import os

def read_kernel():
    path, fname = os.path.split(__file__)
    f = open(os.path.join(path, 'vdW_kernel_table'))
    Nalpha, Nr = map(int, f.readline().strip().split())
    Nr += 1
    rcut = float(f.readline().strip())
    data = []

    for line in f.readlines():
        data += map(float, line.strip().split())

    num = iter(data).next
    qmesh = [ num() for i in range(Nalpha) ]

    kernel = np.zeros((Nalpha, Nalpha, Nr))
    for a1 in range(Nalpha):
        for a2 in range(a1+1):
            kernel[a1, a2, :] = np.array([num() for i in range(Nr)])
            if a1 != a2:
                kernel[a2, a1, :] = kernel[a1, a2, :].copy()
    
    return qmesh, kernel, rcut, Nr

def get_q0_on_grid(n_g, a2_g):
    nmin = np.min(n_g)
    if nmin < 0.0:
        warnings.warn('minimum density %e < 0' % nmin)
    n_g = n_g.clip(1e-10, np.inf)
    kF = (3.0 * np.pi**2 * n_g)**(1.0 / 3.0)
    #assert (n_g > 0.0).all()
    r_s = (3.0 / (4.0 * np.pi * n_g))**(1.0 / 3.0)
    sqrt_r_s = np.sqrt(r_s)

    q_cut = 5.0 # XXX
    q_min = 1e-5 # XXX
    m_cut = 12

    q0 = q_cut * np.ones_like(n_g)
    dq0_drho = np.zeros_like(n_g)
    dq0_dgradrho = np.zeros_like(n_g)
    
    epsr = 1e-12

    Z_ab = -0.8491
    gradient_correction = -Z_ab / (36.0 * kF * n_g**2) * a2_g

    LDA_A = 0.031091
    LDA_a1 = 0.2137
    LDA_b1 = 7.5957
    LDA_b2 = 3.5876
    LDA_b3 = 1.6382
    LDA_b4 = 0.49294

    LDA_1 = 8.0 * np.pi / 3.0 * (LDA_A * (1.0 + LDA_a1 * r_s))
    LDA_2 = 2.0 * LDA_A * (LDA_b1 * sqrt_r_s + LDA_b2 * r_s +
                           LDA_b3 * r_s * sqrt_r_s + LDA_b4 * r_s**2)

    q = kF + LDA_1 * np.log(1.0 + 1.0 / LDA_2) + gradient_correction
    
    dq0_dq = np.zeros_like(n_g)
    exponent = np.zeros_like(n_g)
    for index in range(1, m_cut + 1):
        exponent += (q / q_cut)**index / index
        dq0_dq += (q / q_cut)**(index - 1)

    mask = n_g >= epsr
    q0[mask] = (q_cut * (1.0 - np.exp(-exponent)))[mask]
    dq0_dq *= np.exp(-exponent)
    
    dq0_drho = dq0_dq * (kF / 3.0 - 7.0 / 3.0 * gradient_correction
                         - 8.0 * np.pi / 9.0 * LDA_A * LDA_a1 * r_s
                         * np.log(1.0 + 1.0 / LDA_2)
                         + LDA_1 / (LDA_2 * (1.0 + LDA_2))
                         * (2.0 * LDA_A * (LDA_b1 / 6.0 * sqrt_r_s
                                           + LDA_b2 / 3.0 * r_s 
                                           + LDA_b3 / 2.0 * r_s * sqrt_r_s
                                           + 2.0 * LDA_b4 / 3.0 * r_s**2)))
     
    dq0_dgradrho = -dq0_dq * 2.0 * Z_ab / (36.0 * kF * n_g)
    return q0, dq0_drho, dq0_dgradrho

class VDW(GGA):
    def __init__(self, gganame='revPBE'):
        if gganame == 'revPBE':
            kernel = LibXC('GGA_X_PBE_R+LDA_C_PW')
        elif gganame == 'PBE':
            kernel = LibXC('GGA_X_PBE+LDA_C_PW')
        else:
            raise ValueError('strange gganame')
        self.gganame = gganame
        GGA.__init__(self, kernel)

    def get_setup_name(self):
        return self.gganame

    def set_grid_descriptor(self, gd):
        GGA.set_grid_descriptor(self, gd)
        k2_Q, _ = construct_reciprocal(gd)
        self.k_Q = k2_Q**0.5
        self.k_Q[0, 0, 0] = 0.0 # XXX construct_reciprocal returns crap

    def initialize(self, density, hamiltonian, wfs, occupations):
        GGA.initialize(self, density, hamiltonian, wfs, occupations)
        self.timer = wfs.timer

        self.qmesh, self.phi_aak, self.rcut, self.Nr = read_kernel()
        self.Nalpha = Nalpha = len(self.qmesh)

        self.phispline_aa = np.empty((Nalpha, Nalpha), dtype=object)
        for a1 in range(Nalpha):
            for a2 in range(Nalpha):
                delta_k = 2.0 * np.pi * self.Nr**0 / self.rcut
                phi = self.phi_aak[a1, a2]
                self.phispline_aa[a1, a2] = ScipySpline(delta_k * np.arange(self.Nr),
                                                        phi, k=3, s=0.0)

        thezeros = np.zeros(Nalpha)
        self.spline_a = []
        for alpha in range(Nalpha):
            thezeros[alpha] = 1.0
            spline = ScipySpline(self.qmesh, thezeros.copy(), k=3, s=0.0)
            thezeros[alpha] = 0.0
            self.spline_a.append(spline)

    def calculate_gga(self, e_g, n_sg, dedn_sg, sigma_xg, dedsigma_xg):
        GGA.calculate_gga(self, e_g, n_sg, dedn_sg, sigma_xg, dedsigma_xg)
        assert len(n_sg) == 1
        q0, dq0_dng, dq0_da2g = get_q0_on_grid(n_sg[0], sigma_xg[0])
        self.q0_g = q0.copy()
        #import pylab as pl
        #pl.plot(dq0_dng.ravel())
        #pl.show()
        #dq0_dng *= 0.
        Nalpha = self.Nalpha
        spline_a = self.spline_a

        thetas_ag = self.gd.zeros(Nalpha)
        thetas_ak = []
        for a, theta_g in enumerate(thetas_ag):
            theta_g.flat = self.spline_a[a](q0.ravel())
            theta_g.flat *= n_sg[0].ravel()
            theta_k = fftn(theta_g, self.gd.N_c).copy() # XXX think: parallel, padding
            thetas_ak.append(theta_k)
        thetas_ak = np.array(thetas_ak)

        Ecnl = 0.0
        F_ag = []
        F_ak = np.zeros_like(thetas_ak)
        for a1 in range(Nalpha):
            for a2 in range(Nalpha):
                theta1 = thetas_ak[a1]
                theta2 = thetas_ak[a2]
                phispline = self.phispline_aa[a1, a2]
                kval = self.k_Q.ravel()
                splineval = phispline(kval)
                tmp_g = theta2.ravel() * splineval
                Ecnl += (theta1.ravel().conj() * tmp_g).sum()
                F_ak[a1].flat += tmp_g

            F_g = ifftn(F_ak[a1])
            F_ag.append(F_g)
        F_ag = np.array(F_ag)

        Ecnl *= 0.5 * self.gd.dv / self.gd.N_c.prod()
        print "Non local energy", Ecnl
        self.Ecnl = Ecnl
        e_g[0, 0, 0] += Ecnl.real / self.gd.dv

        _v = np.zeros_like(q0)
        
        self.grrr = []
        for a in range(Nalpha):
            splineval, splineder = np.array([spline_a[a].derivatives(x)[:2]
                                             for x in q0.ravel()]).T.copy()
            dpadq0_g = np.zeros_like(q0)
            dpadq0_g.flat = splineder

            dthetatmp_g = n_sg[0] * dpadq0_g #* self.dhdx_g
            dthetaadn_g = np.zeros_like(q0)
            dthetaadn_g.flat[:] = dq0_dng #1.#splineval + (dq0_dng * dthetatmp_g).ravel()
            _v += (dthetaadn_g * F_ag[a]**0).real
            self.grrr.append(F_ag[a])
            #dthetaada2_g = 1.#dq0_da2g * dthetatmp_g
            #dedsigma_xg[0] += (dthetaada2_g * F_ag[a]).real
        self.vnl_g = _v
        dedn_sg[0] += _v

if 0:
    from ase import Atoms
    from gpaw import GPAW
    from gpaw.occupations import FermiDirac
    from ase.io.cube import read_cube
    from ase.units import Bohr
    from gpaw.eigensolvers.cg import CG

    """
    atoms = Atoms('He')
    atoms.set_cell((4.1*Bohr, 4.1*Bohr, 4.1*Bohr))
    atoms.set_pbc((1,1,1))
    calc = GPAW(xc=VDW(), gpts=(16,16,16), kpts=(1,1,1),maxiter=4)
    atoms.set_calculator(calc)
    try:
        atoms.get_potential_energy()
    except:
        pass

    n_g, atoms = read_cube('density.txt.cube',
                           read_data=True)
    print calc.hamiltonian.xc.calculate(calc.density.finegd, n_g.reshape((1,32,32,32)))

    print n_g.shape

    raise SystemExit
    """

    a0=0.52917721092
    for i in range(1,6):
        alat = 4.6767 * a0
        cell = alat * np.array( [ [ 1.000000,   0.000000,   0.000000 ],
                                [ -0.500000,   0.866025,   0.000000  ],
                                [ 0.000000,   0.000000,   2.517500+0.1*i ] ])

        atoms = Atoms('C4',[     
                      (0.0,0.0,0.0),
                      (1.0/3.0,2.0/3.0,0.0),
                      (0.0,0.0,0.5),
                      (2.0/3.0,1.0/3.0,0.5)
                      ],
                      pbc=(1,1,1))
        atoms.set_cell(cell, scale_atoms=True)
        from gpaw.xc.vdw import FFTVDWFunctional
        xc = FFTVDWFunctional(soft_correction=True)
        #xc=VDW()
        calc = GPAW(xc=xc, eigensolver=CG(niter=10), 
                     kpts=(2,2,2),
                     gpts=(24,24,64), occupations=FermiDirac(0.05))
        atoms.set_calculator(calc)
        atoms.get_potential_energy()

        print "Enl", xc.get_Ecnl()

    """
    atoms = Atoms('He')
    atoms.set_cell([2,2,2])
    atoms.set_pbc((True,True,True))
    #from gpaw.xc.vdw import FFTVDWFunctional
    #calc = GPAW(gpts=(12,12,12), xc=FFTVDWFunctional())
    calc = GPAW(gpts=(12,12,12), xc=VDW())
    atoms.set_calculator(calc)
    atoms.get_potential_energy()
    """
