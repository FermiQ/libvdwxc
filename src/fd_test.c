#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>

#include "vdwxc.h"

#if defined HAVE_CONFIG_H
#include "config.h"
#endif

#include "vdw_core.h"
#include "vdw_q0.h"

void zero(int ngpts, double* arr)
{
    int i;
    for(i = 0; i < ngpts; i++) {
        arr[i] = 0.0;
    }
}

void reset(int ngpts, double* rho_i, double* sigma_i,
           double* dedn_i, double* dedsigma_i)
{
    int i;
    for(i = 0; i < ngpts; i++) {
        rho_i[i] = 1.3;//1e-4;//10;//0.0001;//(1.2 + sin(9 * i)) * .3;
        sigma_i[i] = 0.7;//(1.3 + cos(5 * i)) * 1.;
    }
    zero(ngpts, dedn_i);
    zero(ngpts, dedsigma_i);
}

double fdcheck(vdwxc_data data, double n, double s, double dx)
{
    int ngpts = data->cell.Nlocal[0] * data->cell.Nlocal[1] * data->cell.Nlocal[2];
    double* n_i = (double*)malloc(ngpts * sizeof(double));
    double* s_i = (double*)malloc(ngpts * sizeof(double));
    double* dedn_i = (double*)malloc(ngpts * sizeof(double));
    double* deds_i = (double*)malloc(ngpts * sizeof(double));


    reset(ngpts, n_i, s_i, dedn_i, deds_i);
    n_i[0] = n;
    s_i[0] = s;

    double energy = vdwxc_calculate(data, n_i, s_i, dedn_i, deds_i);

    double v = dedn_i[0];
    double vs = deds_i[0];

    n_i[0] -= dx / 2.;
    double e_nminus = vdwxc_calculate(data, n_i, s_i, dedn_i, deds_i);
    n_i[0] += dx;
    double e_nplus = vdwxc_calculate(data, n_i, s_i, dedn_i, deds_i);

    double dedn_fd = (e_nplus - e_nminus) / dx;
    double v_fd = dedn_fd / data->cell.dV;
    double err = v_fd - v;

    n_i[0] = n;
    s_i[0] -= dx / 2.;
    double e_sminus = vdwxc_calculate(data, n_i, s_i, dedn_i, deds_i);
    s_i[0] += dx;
    double e_splus = vdwxc_calculate(data, n_i, s_i, dedn_i, deds_i);
    double deds_fd = (e_splus - e_sminus) / dx;
    double vs_fd = deds_fd / data->cell.dV;
    double s_err = vs_fd - vs;

    printf("dx=%.2e energy=%e ", dx, energy);
    printf("rho=%f sigma=%f ", n, s_i[0]);
    printf("v=%e, v[fd]=%e rel=%e err=%.3e\n", v, v_fd, v_fd / v, err);
    printf("       vs=%e, vs[fd]=%e rel=%e err=%.3e\n",
           vs, vs_fd, vs_fd / vs, s_err);

    free(n_i);
    free(s_i);
    free(dedn_i);
    free(deds_i);

    return energy;
}


void test_q0_derivative(double rho, double sigma, double drho, double dsigma) {
    int Ng = 1;
    double rho_g[Ng];
    double sigma_g[Ng];
    double q0_g[Ng];
    double dq0_drho_g[Ng];
    double dq0_dsigma_g[Ng];

    int i;
    for(i=0; i < Ng; i++) {
        rho_g[i] = rho;
        sigma_g[i] = 0.0;
    }

    double Z_ab = 1.0;
    double q_cut = 5.0;
    vdwxc_calculate_q0(Ng, Z_ab, q_cut,
                       rho_g, sigma_g, q0_g,
                       dq0_drho_g, dq0_dsigma_g);

    double q0 = q0_g[0];
    double dq0_drho = dq0_drho_g[0];
    double dq0_dsigma = dq0_dsigma_g[0];

    rho_g[0] -= drho;
    sigma_g[0] -= dsigma;

    vdwxc_calculate_q0(Ng, Z_ab, q_cut, rho_g, sigma_g, q0_g, dq0_drho_g, dq0_dsigma_g);
    double q0_left = q0_g[0];
    rho_g[0] += 2 * drho;
    sigma_g[0] += 2 * dsigma;
    vdwxc_calculate_q0(Ng, Z_ab, q_cut, rho_g, sigma_g, q0_g, dq0_drho_g, dq0_dsigma_g);
    double q0_right = q0_g[0];

    printf("rho=%f sigma=%f q0=%f", rho, sigma, q0);
    if(drho > 0) {
        double dq0_drho_fd = (q0_right - q0_left) / (2 * drho);
        double err = dq0_drho_fd - dq0_drho;
        double rel = dq0_drho_fd / dq0_drho;
        printf(" dq0_drho ana=%f vs fd=%f rel=%f err=%e",
               dq0_drho, dq0_drho_fd, rel, err);
        // The cutoff is not entirely good; we get an error of 7e-4
        // for very small sigma.  We could do better.
    }
    if(dsigma > 0) {
        double dq0_dsigma_fd = (q0_right - q0_left) / (2 * dsigma);
        double err = dq0_dsigma_fd - dq0_dsigma;
        double rel = dq0_dsigma_fd / dq0_dsigma;
        printf(" dq0_drho ana=%f vs fd=%f rel=%f err=%e",
               dq0_dsigma, dq0_dsigma_fd, rel, err);
    }
    printf("\n");
}

void test_many_q0_derivatives() {
    int i = 0;
    printf("small rho\n");
    printf("=========\n");
    for(i = 1; i < 20; i++) {
        double rho = exp(-2 * i);
        test_q0_derivative(rho, 1.2, rho / 1e5, 0.0);
    }

    printf("\n");
    printf("large rho\n");
    printf("=========\n");
    for(i = 1; i < 100; i++) {
        double frac = i / 10.0;
        double rho = frac * frac;
        test_q0_derivative(rho, 1.2, rho / 1e5, 0.0);
    }

    printf("\n");
    printf("small sigma\n");
    printf("===========\n");
    for(i = 1; i < 100; i++) {
        double sigma = exp(-2 * i);
        test_q0_derivative(2.7, sigma, 0.0, sigma / 1e5);
    }

    printf("\n");
    printf("large sigma\n");
    printf("===========\n");
    for(i = 1; i < 100; i++) {
        double sigma = 0.3 * i;
        test_q0_derivative(2.3, sigma, 0.0, sigma / 1e5);
    }

}


void more_elaborate_derivatives(int argc, char** argv) {
    vdwxc_data vdw = vdwxc_new(FUNC_VDWDF);
    int dim1 = 1;
    int dim2 = 1;
    int dim3 = 2;

    vdwxc_set_unit_cell(vdw,
                        dim1, dim2, dim3,
                        8.1,0.0,0.0,
                        0.0,7.9,0.0,
                        0.0,0.0,11.5);
    vdwxc_init_serial(vdw);

    vdwxc_print(vdw);

    int i;
    double n0 = 2.0, s0 = 1.2;
    double n, s;
    double dx = 1e-6;
    double e;
    for(i=1; i < 100; i++) {
        n = 0.3 * i;
        e = fdcheck(vdw, n, s0, dx);
        if(e > 1e10) {
            break;
        }
    }
    for(i=1; i < 100; i++) {
        s = 0.1 * i;
        e = fdcheck(vdw, n0, s, dx);
    }

    vdwxc_finalize(&vdw);
}

int main(int argc, char** argv) {
    //test_many_q0_derivatives();
    more_elaborate_derivatives(argc, argv);
    return 0;
}
