#include <math.h>
#include <assert.h>
#include <stdio.h>

#if defined HAVE_CONFIG_H
#include "config.h"
#endif

#include "vdw_kernel.h"

#include "vdw_kernel.inc"

static double qmesh[20] = {
   1.00000000000000E-05,   4.49420825586261E-02,   9.75593700991365E-02,   1.59162633466142E-01,
   2.31286496836006E-01,   3.15727667369529E-01,   4.14589693721418E-01,   5.30335368404141E-01,
   6.65848079422965E-01,   8.24503639537924E-01,   1.01025438252095E+00,   1.22772762136457E+00,
   1.48234092117491E+00,   1.78043705835953E+00,   2.12944202813364E+00,   2.53805003653458E+00,
   3.01644008535668E+00,   3.57652954544246E+00,   4.23227103519872E+00,   5.00000000000000E+00 };

static double vdw_splinedata[20*19*4] = {
#include "vdw_splines.inc"
};


void vdwxc_evaluate_palpha_splines_derivative(struct vdwxc_kernel *kernel, double q, double* output)
{
    int alpha;
    int beta;
    if (q > kernel->qmesh_a[kernel->nalpha - 1]) {
        assert(0);
    } else if (q <= kernel->qmesh_a[0]) {
        q = kernel->qmesh_a[0];
        alpha = 0;
    } else {
        // Loop over spline regions, This is O(N) algorithm, but should be fine anyway
        for (alpha=1; alpha < kernel->nalpha; alpha++) {
            if (qmesh[alpha] >= q) {
                alpha--;
                break;
            }
        }
    }
    // alpha contains now the left boundary
    double dq = q-qmesh[alpha];

    for (beta=0; beta < kernel->nalpha; beta++) {
        const double* polynomial = vdw_splinedata + beta * kernel->nregions * kernel->npalphacoefs +
            alpha * kernel->npalphacoefs;
        *output++ = polynomial[1] + 2.0*dq*polynomial[2] + 3.0*dq*dq*polynomial[3];
    }
}

void vdwxc_evaluate_palpha_splines(struct vdwxc_kernel *kernel, double prefactor, double q, double* output)
{
  int alpha;
  int beta;
  if (q > kernel->qmesh_a[kernel->nalpha - 1]) {
      assert(0);
  } else if (q <= kernel->qmesh_a[0]) {
      q = kernel->qmesh_a[0];
      alpha = 0;
  } else {
      // Loop over spline regions, This is O(N) algorithm, but should be fine anyway
      for (alpha=1; alpha < kernel->nalpha; alpha++) {
          if (qmesh[alpha] >= q) {
              alpha--;
              break;
          }
      }
  }
  // alpha contains now the left boundary
  double dq = q-qmesh[alpha];

  for (beta=0; beta < kernel->nalpha; beta++) {
     const double* polynomial = vdw_splinedata + beta * kernel->nregions * kernel->npalphacoefs +
                                           alpha * kernel->npalphacoefs;

     *output++ = prefactor * (polynomial[0] + dq*(polynomial[1] + dq*(polynomial[2] + dq*polynomial[3])));
  }
}


void vdwxc_interpolate_kernels(struct vdwxc_kernel *kernel,
                               double k, double* kernel_aa)
{
    assert(kernel->nkernelcoefs == 1);
    int a1;
    int a2;
    // Linear interpolation
    double index = k / kernel->deltak;
    int istart = floor(index);
    int asqr = kernel->nalpha * kernel->nalpha;

    if (k < 0 || istart > kernel->nkpoints - 2) {
        for (a1=0; a1 < asqr; a1++) {
            *kernel_aa++=0;
        }
        return;
    }
    index -= istart;
    double index2 = 1.0 - index;
    double* left = (double*)kernel_kaa + istart * asqr;
    double* right = (double*)kernel_kaa + (istart + 1) * asqr;
    for (a1=0; a1 < kernel->nalpha; a1++) {
        for (a2=0; a2 < kernel->nalpha; a2++) {
            *kernel_aa++ = (*left++) * index2 + (*right++) * index;
        }
    }
}

struct vdwxc_kernel vdwxc_new_kernel(int nalpha, int nregions,
                                     int npalphacoefs, int nkernelcoefs,
                                     int nkpoints, double deltak,
                                     double *qmesh_a,
                                     double *splines_arc,
                                     double *kernel_aakc)
{
    struct vdwxc_kernel kernel;
    kernel.nalpha = nalpha;
    kernel.nregions = nregions;
    kernel.npalphacoefs = npalphacoefs;
    kernel.nkernelcoefs = nkernelcoefs;
    kernel.nkpoints = nkpoints;
    kernel.deltak = deltak;

    kernel.qmesh_a = qmesh_a;
    kernel.splines_arc = splines_arc;
    kernel.kernel_aakc = kernel_aakc;
    return kernel;
}

struct vdwxc_kernel vdwxc_default_kernel()
{
    return vdwxc_new_kernel(20, 19, 4, 1, 1025, 6.283185307179587e-02,
                            qmesh, vdw_splinedata, kernel_kaa);
}
