# -*- Autoconf -*-
#
# Copyright (C) 2015 Yann Pouillon
#
# This file is part of the Libvdwxc software package. For license information,
# please see the COPYING file in the top-level directory of the Libvdwxc source
# distribution.
#



# VDW_PFFT_DETECT()
# -----------------
#
# Check whether the PFFT library is working.
#
AC_DEFUN([VDW_PFFT_DETECT],[
  dnl Init
  vdw_pfft_ok="unknown"
  vdw_pfft_has_hdrs="unknown"
  vdw_pfft_has_libs="unknown"

  dnl Backup environment
  vdw_saved_CPPFLAGS="${CPPFLAGS}"
  vdw_saved_LIBS="${LIBS}"

  dnl Prepare build parameters
  CPPFLAGS="${CPPFLAGS} ${vdw_pfft_incs}"
  LIBS="${vdw_pfft_libs} ${LIBS}"

  dnl Look for C includes
  AC_LANG_PUSH([C])
  AC_CHECK_HEADERS([pfft.h],
    [vdw_pfft_has_hdrs="yes"], [vdw_pfft_has_hdrs="no"])
  AC_LANG_POP([C])

  dnl Look for C libraries and routines
  if test "${vdw_pfft_has_hdrs}" = "yes"; then
    AC_LANG_PUSH([C])
    AC_MSG_CHECKING([whether the PFFT libraries work])
    AC_LINK_IFELSE([AC_LANG_PROGRAM(
      [[
#include <pfft.h>
      ]],
      [[
        pfft_init();
      ]])], [vdw_pfft_has_libs="yes"], [vdw_pfft_has_libs="no"])
    AC_MSG_RESULT([${vdw_pfft_has_libs}])
    AC_LANG_POP([C])
  fi

  dnl Take final decision
  AC_MSG_CHECKING([whether we have a full PFFT support])
  if test "${vdw_pfft_has_hdrs}" = "yes" -a \
          "${vdw_pfft_has_libs}" = "yes"; then
    vdw_pfft_ok="yes"
  else
    vdw_pfft_ok="no"
  fi
  AC_MSG_RESULT([${vdw_pfft_ok}])

  dnl Restore environment
  CPPFLAGS="${vdw_saved_CPPFLAGS}"
  LIBS="${vdw_saved_LIBS}"
]) # VDW_PFFT_DETECT
